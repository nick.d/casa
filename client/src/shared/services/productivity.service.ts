import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { EnvService } from './env/env.service';
import { RegisterWorkParams } from '../types/productivity/register-work-params';
import { WorkSummary } from '../types/productivity/work-summary';

@Injectable({
  providedIn: 'root',
})
export class ProductivityService {
  private _url = this._env.apiUrl + 'api/';

  constructor(private _http: HttpClient, private _env: EnvService) {}

  weeklyOverview(): Observable<WorkSummary[]> {
    return this._http.get<WorkSummary[]>(
      `${this._url}productivity/weekly-overview`,
    );
  }

  overview(params?: HttpParams): Observable<WorkSummary[]> {
    return this._http.get<WorkSummary[]>(
      `${this._url}productivity/overview`,
      params
        ? {
            params,
          }
        : undefined,
    );
  }

  register(params: RegisterWorkParams): Observable<boolean> {
    return this._http.post<boolean>(
      `${this._url}productivity/register`,
      params,
    );
  }
}
