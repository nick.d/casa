import { TestBed } from '@angular/core/testing';

import { ProductivityService } from './productivity.service';

describe('ProductivityService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductivityService = TestBed.get(ProductivityService);
    expect(service).toBeTruthy();
  });
});
