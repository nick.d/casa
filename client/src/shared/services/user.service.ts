import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EnvService } from './env/env.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _url = this._env.apiUrl + 'api/';

  constructor(private _http: HttpClient, private _env: EnvService) {}

  getUser(): Observable<any> {
    return this._http.get<any>(`${this._url}user`);
  }
}
