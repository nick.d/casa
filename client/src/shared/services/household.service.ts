import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { CreateHouseholdFormParams } from 'src/shared/types/join/create-household-form-params';
import { HouseholdCreatedParams } from 'src/shared/types/join/household-created-params';
import { JoinHouseholdParams } from '../types/join/join-household-params';
import { HouseholdJoinedParams } from '../types/join/household-joined-params';
import { EnvService } from './env/env.service';
import { JoinHouseholdAgainParams } from '../types/join/join-household-again-params';
import { HouseholdInfo } from '../types/household/household-info';

@Injectable({
  providedIn: 'root',
})
export class HouseholdService {
  private _url = this._env.apiUrl + 'api/';

  constructor(
    private _http: HttpClient,
    private _auth: AuthService,
    private _env: EnvService,
  ) {}

  info(): Observable<HouseholdInfo> {
    return this._http.get<HouseholdInfo>(`${this._url}household/info`);
  }

  getHousehold(): Observable<any> {
    return this._http.get<HouseholdInfo>(`${this._url}household`);
  }

  join(params: JoinHouseholdParams): Observable<HouseholdJoinedParams> {
    return this._http.post(`${this._url}household/join`, params).pipe(
      tap((returnParams: HouseholdJoinedParams) =>
        this._auth.setToken(returnParams.token),
      ),
      map((returnParams: HouseholdJoinedParams) => returnParams),
    );
  }

  joinAgain(
    params: JoinHouseholdAgainParams,
  ): Observable<HouseholdJoinedParams> {
    return this._http.post(`${this._url}household/join-again`, params).pipe(
      tap((returnParams: HouseholdJoinedParams) =>
        this._auth.setToken(returnParams.token),
      ),
      map((returnParams: HouseholdJoinedParams) => returnParams),
    );
  }

  create(
    params: CreateHouseholdFormParams,
  ): Observable<HouseholdCreatedParams> {
    return this._http.post(`${this._url}household/create`, params).pipe(
      tap((returnParams: HouseholdCreatedParams) =>
        this._auth.setToken(returnParams.token),
      ),
      map((returnParams: HouseholdCreatedParams) => returnParams),
    );
  }
}
