import { Component, OnInit } from "@angular/core";
import { SnackbarRef } from "../../types/snackbar-ref";
import {
  SnackbarParamters,
  SnackbarConfig,
  SnackbarType
} from "../../types/snackbar.type";
import { trigger, transition, style, animate } from "@angular/animations";

@Component({
  selector: "app-snackbar",
  templateUrl: "./snackbar.component.html",
  styleUrls: ["./snackbar.component.scss"],
  animations: [
    trigger("myInsertRemoveTrigger", [
      transition(":enter", [
        style({ opacity: 0, transform: "translateY(2.5rem)" }),
        animate(
          "0.2s ease-out",
          style({ opacity: 1, transform: "translateY(0)" })
        )
      ])
    ])
  ]
})
export class SnackbarComponent implements OnInit {
  data: SnackbarParamters;
  config: SnackbarConfig;
  typeMap = new Map([
    [SnackbarType.SUCCESS, "check_circle"],
    [SnackbarType.INFO, "info"],
    [SnackbarType.WARNING, "warning"],
    [SnackbarType.ERROR, "error"]
  ]);

  constructor(private _snackbarRef: SnackbarRef) {
    this.data = _snackbarRef.data;
    this.config = _snackbarRef.config;
  }

  ngOnInit() {}

  onClose() {
    this._snackbarRef.dismiss();
  }
}
