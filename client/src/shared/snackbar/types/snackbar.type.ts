export interface SnackbarConfig {
  duration: number;
  type?: SnackbarType;
  theme?: SnackbarTheme;
}

export enum SnackbarType {
  INFO = "info",
  WARNING = "warning",
  ERROR = "error",
  SUCCESS = "success"
}

export enum SnackbarTheme {
  BLUE = "blue",
  PURPLE = "purple",
  ORANGE = "orange",
  GREEN = "green",
  SUCCESS = "success",
  WARNING = "warning",
  ERROR = "error"
}

export interface SnackbarParamters {
  message: string;
}
