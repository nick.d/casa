import { OverlayRef } from "@angular/cdk/overlay";
import { Subject } from "rxjs";
import { SnackbarParamters, SnackbarConfig } from "./snackbar.type";

export class SnackbarRef {
  private _afterClosed$ = new Subject<void>();
  private _durationTimeoutId: number;
  afterClosed$ = this._afterClosed$.asObservable();

  constructor(
    private overlay: OverlayRef,
    public data: SnackbarParamters,
    public config?: SnackbarConfig
  ) {}

  dismiss(): void {
    clearTimeout(this._durationTimeoutId);
    this.overlay.dispose();
    this._afterClosed$.next();
    this._afterClosed$.complete();
  }

  dismissAfter(duration: number) {
    this._durationTimeoutId = window.setTimeout(() => this.dismiss(), duration);
  }
}
