import { OverlayModule } from "@angular/cdk/overlay";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatIconModule } from "@angular/material/icon";

import { SnackbarComponent } from "./containers/snackbar/snackbar.component";
import { SnackbarService } from "./services/snackbar.service";

@NgModule({
  declarations: [SnackbarComponent],
  imports: [CommonModule, OverlayModule, MatIconModule],
  entryComponents: [SnackbarComponent],
  providers: [SnackbarService]
})
export class SnackbarModule {}
