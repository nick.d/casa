import { Overlay, OverlayRef } from "@angular/cdk/overlay";
import { ComponentPortal, PortalInjector } from "@angular/cdk/portal";
import { Injectable, Injector } from "@angular/core";

import { SnackbarComponent } from "../containers/snackbar/snackbar.component";
import { SnackbarRef } from "../types/snackbar-ref";
import { SnackbarConfig, SnackbarParamters } from "../types/snackbar.type";

@Injectable()
export class SnackbarService {
  constructor(private overlay: Overlay, private injector: Injector) {}

  open(message: string, snackbarConfig?: SnackbarConfig): SnackbarRef {
    const overlayRef = this._getOverlayRef();

    const snackbarRef = this._attach(overlayRef, { message }, snackbarConfig);

    if (snackbarConfig && snackbarConfig.duration) {
      snackbarRef.dismissAfter(snackbarConfig.duration);
    }

    return snackbarRef;
  }

  private _attach(
    overlay: OverlayRef,
    data: SnackbarParamters,
    config?: SnackbarConfig
  ) {
    const snackbarRef = new SnackbarRef(overlay, data, config);
    const tokens = new WeakMap();
    tokens.set(SnackbarRef, snackbarRef);

    const portalInjector = new PortalInjector(this.injector, tokens);
    overlay.attach(
      new ComponentPortal(SnackbarComponent, null, portalInjector)
    );

    return snackbarRef;
  }

  private _getOverlayRef() {
    const positionStrategy = this.overlay.position().global();
    positionStrategy.left("0");
    positionStrategy.bottom("0");

    return this.overlay.create({
      hasBackdrop: false,
      positionStrategy
    });
  }
}
