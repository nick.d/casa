import { Component } from "@angular/core";

import { ConfirmDialogParameters } from "../../types/confirm-dialog-paramaters";
import { DialogRef } from "../../types/dialog-ref";

@Component({
  selector: "app-confirm-dialog",
  templateUrl: "./confirm-dialog.component.html",
  styleUrls: ["./confirm-dialog.component.scss"]
})
export class ConfirmDialogComponent {
  data: ConfirmDialogParameters;
  theme: string;

  constructor(private _dialogRef: DialogRef) {
    this.data = _dialogRef.data;
    this.theme = _dialogRef.config.theme;
  }

  onAccept() {
    this._dialogRef.close("accepted");
  }

  onDecline() {
    this._dialogRef.close("declined");
  }
}
