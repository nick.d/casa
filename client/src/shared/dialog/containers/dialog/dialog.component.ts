import { Component, OnInit, TemplateRef, Type, ViewChild } from "@angular/core";
import { DialogRef } from "../../types/dialog-ref";
import { trigger, transition, animate, style } from "@angular/animations";

@Component({
  selector: "app-dialog",
  templateUrl: "./dialog.component.html",
  styleUrls: ["./dialog.component.scss"],
  animations: [
    trigger("myInsertRemoveTrigger", [
      transition(":enter", [
        style({ opacity: 0, transform: "translateY(calc(-50% - 3.5rem))" }),
        animate(
          "0.2s ease-out",
          style({ opacity: 1, transform: "translateY(calc(-50% - 1rem))" })
        )
      ])
    ])
  ]
})
export class DialogComponent implements OnInit {
  @ViewChild("content", { read: TemplateRef, static: false })
  content: TemplateRef<any>;
  contentComponent: Type<any>;
  type: string;
  theme: string;

  constructor(private _dialogRef: DialogRef) {
    this.contentComponent = _dialogRef.contentComponent;
    this.type = _dialogRef.config.type;
    this.theme = _dialogRef.config.theme;
  }

  ngOnInit() {}
}
