import { Subject } from "rxjs";
import { OverlayRef } from "@angular/cdk/overlay";
import {
  DialogCloseEvent,
  DialogContentComponent,
  DialogType
} from "./dialog.type";
import { take } from "rxjs/operators";

export class DialogRef<T = any> {
  private afterClosed = new Subject<DialogCloseEvent>();
  afterClosed$ = this.afterClosed.asObservable();

  constructor(
    private overlay: OverlayRef,
    public contentComponent: DialogContentComponent,
    public data: T,
    public config: DialogType
  ) {
    overlay
      .backdropClick()
      .pipe(take(1))
      .subscribe(() => {
        if (config.closeOnBackdropClick) {
          this._close("backdropClick", null);
        }
      });
  }

  close(reason: string, data?: any) {
    this._close("close", reason, data);
  }

  private _close(type: DialogCloseEvent["type"], reason: string, data?: any) {
    this.overlay.dispose();
    this.afterClosed.next({
      type,
      reason,
      data: data ? data : null
    });
    this.afterClosed.complete();
  }
}
