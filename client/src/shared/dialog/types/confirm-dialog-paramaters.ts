export interface ConfirmDialogParameters {
  title: string;
  message: string;
  decline: string;
  accept: string;
}
