import { Type } from "@angular/core";

export interface DialogParameters<T> {
  contentComponent: DialogContentComponent;
  data?: T;
  config?: DialogType;
}

export type DialogContentComponent = Type<any>;

export interface DialogType {
  closeOnBackdropClick: boolean;
  type: string;
  theme: "blue" | "purple" | "orange" | "green";
  hasBackdrop?: boolean;
  position?: DialogPosition;
}

export interface DialogPosition {
  readonly connectedElement: HTMLElement;
  readonly xOffset?: number;
  readonly yOffset?: number;
}

export interface DialogCloseEvent {
  type: "backdropClick" | "close";
  reason: string;
  data?: any;
}
