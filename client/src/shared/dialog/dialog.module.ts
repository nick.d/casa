import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { DialogComponent } from "./containers/dialog/dialog.component";
import { DialogService } from "./services/dialog.service";
import { PortalModule } from "@angular/cdk/portal";
import { OverlayModule } from "@angular/cdk/overlay";
import { ConfirmDialogComponent } from "./components/confirm-dialog/confirm-dialog.component";
import { MatIconModule } from "@angular/material/icon";

@NgModule({
  declarations: [DialogComponent, ConfirmDialogComponent],
  imports: [CommonModule, OverlayModule, PortalModule, MatIconModule],
  providers: [DialogService],
  exports: [DialogComponent, ConfirmDialogComponent],
  entryComponents: [DialogComponent, ConfirmDialogComponent]
})
export class DialogModule {}
