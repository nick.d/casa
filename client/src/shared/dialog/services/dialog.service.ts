import { Injectable, Injector } from "@angular/core";
import { Overlay } from "@angular/cdk/overlay";
import { DialogRef } from "../types/dialog-ref";
import { ComponentPortal, PortalInjector } from "@angular/cdk/portal";
import { DialogComponent } from "../containers/dialog/dialog.component";
import {
  DialogParameters,
  DialogPosition,
  DialogType
} from "../types/dialog.type";

@Injectable()
export class DialogService {
  constructor(private overlay: Overlay, private injector: Injector) {}

  private static _createInjector(dialogRef: DialogRef, injector: Injector) {
    const tokens = new WeakMap();
    tokens.set(DialogRef, dialogRef);
    return new PortalInjector(injector, tokens);
  }

  open<T>({
    contentComponent,
    data,
    config
  }: DialogParameters<T>): DialogRef<T> {
    const configuration = config
      ? config
      : {
          closeOnBackdropClick: true,
          type: "normal",
          theme: null
        };
    const overlayRef = this._getOverlayRef(configuration);
    const dialogRef = new DialogRef<T>(
      overlayRef,
      contentComponent,
      data,
      configuration
    );

    const portalInjector = DialogService._createInjector(
      dialogRef,
      this.injector
    );
    overlayRef.attach(
      new ComponentPortal(DialogComponent, null, portalInjector)
    );

    return dialogRef;
  }

  private _getOverlayRef(config: DialogType) {
    if (!config.position) {
      return this.overlay.create({
        hasBackdrop:
          config.hasBackdrop === null || config.hasBackdrop === undefined
            ? true
            : config.hasBackdrop
      });
    } else {
      return this.overlay.create({
        hasBackdrop:
          config.hasBackdrop === null || config.hasBackdrop === undefined
            ? true
            : config.hasBackdrop,
        positionStrategy: this._setPosition(config.position),
        scrollStrategy: this.overlay.scrollStrategies.block()
      });
    }
  }

  private _setPosition(positionConfig: DialogPosition) {
    const position = this.overlay
      .position()
      .flexibleConnectedTo(positionConfig.connectedElement)
      .withPositions([
        {
          originX: "start",
          originY: "center",
          overlayX: "end",
          overlayY: "center"
        }
      ]);
    if (positionConfig.xOffset) {
      position.withDefaultOffsetX(positionConfig.xOffset);
    }
    if (positionConfig.yOffset) {
      position.withDefaultOffsetY(positionConfig.yOffset);
    }
    return position;
  }
}
