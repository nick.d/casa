import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment } from '@angular/router';

import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanLoad {
  constructor(private _auth: AuthService, private _router: Router) {}

  canLoad(route: Route, segments: UrlSegment[]): boolean {
    const isSignedIn = !!this._auth.getToken();

    if (!isSignedIn) {
      this._router.navigate(['join']);
    }

    return isSignedIn;
  }
}
