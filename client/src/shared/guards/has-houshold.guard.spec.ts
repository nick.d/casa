import { TestBed, async, inject } from '@angular/core/testing';

import { HasHousholdGuard } from './has-houshold.guard';

describe('HasHousholdGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HasHousholdGuard]
    });
  });

  it('should ...', inject([HasHousholdGuard], (guard: HasHousholdGuard) => {
    expect(guard).toBeTruthy();
  }));
});
