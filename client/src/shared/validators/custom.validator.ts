import { FormControl, Validators } from "@angular/forms";

export class CustomValidators {
  static requiredIf(condition: () => any) {
    return (formControl: FormControl) => {
      if (!formControl.parent) {
        return null;
      }

      if (condition()) {
        return Validators.required(formControl);
      }
      return null;
    };
  }
}
