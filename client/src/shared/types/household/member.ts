export interface Member {
  username: string;
  email: string;
}
