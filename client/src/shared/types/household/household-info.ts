import { Member } from './member';

export interface HouseholdInfo {
  name: string;
  code: string;
  username: string;
  email: string;
  members: Member[];
}
