export interface WorkSummary {
  user: string;
  totalMinutes: number;
}
