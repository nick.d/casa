export interface RegisterWorkParams {
  description: string;
  durationInMinutes: number;
}
