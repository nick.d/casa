export interface JoinHouseholdParams {
  householdCode: string;
  username: string;
  email: string;
}
