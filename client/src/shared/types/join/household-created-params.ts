export interface HouseholdCreatedParams {
  householdCode: string;
  token: string;
}
