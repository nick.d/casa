export interface CreateHouseholdFormParams {
  householdName: string;
  username: string;
  email: string;
}
