export interface HouseholdJoinedParams {
  householdName: string;
  token: string;
}
