import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CannotLoadComponent } from './cannot-load.component';

describe('CannotLoadComponent', () => {
  let component: CannotLoadComponent;
  let fixture: ComponentFixture<CannotLoadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CannotLoadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CannotLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
