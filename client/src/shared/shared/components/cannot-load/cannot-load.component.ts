import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-cannot-load',
  templateUrl: './cannot-load.component.html',
  styleUrls: ['./cannot-load.component.scss'],
})
export class CannotLoadComponent implements OnInit {
  @Output() refresh = new EventEmitter<void>();
  @Input() message: string;

  constructor() {}

  ngOnInit() {}

  onRefresh() {
    this.refresh.next();
  }
}
