import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Observable, merge } from 'rxjs';
import { mapTo, debounceTime, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-get-started',
  templateUrl: './get-started.component.html',
  styleUrls: ['./get-started.component.scss'],
})
export class GetStartedComponent implements OnInit {
  @Input() householdCode: string;
  @Input() householdName: string;
  @Input() hasMargin = false;
  copiedToClipboardTooltipVisible$: Observable<boolean>;
  private _triggerTooltip$ = new Subject<void>();
  constructor(private _router: Router) {}

  ngOnInit() {
    this._initStreams();
  }

  private _initStreams() {
    this.copiedToClipboardTooltipVisible$ = merge(
      this._triggerTooltip$.pipe(debounceTime(5000), mapTo(false)),
      this._triggerTooltip$.pipe(mapTo(true)),
    ).pipe(startWith(false));
  }

  async copyToClipboard(code: string) {
    if (navigator.clipboard && code) {
      try {
        await navigator.clipboard.writeText(this.createCurrentShareUrl(code));
        this._triggerTooltip$.next();
      } catch (ex) {
        console.log(ex);
      }
    }
  }

  canUseShareApi(): boolean {
    return !!(navigator as any).share;
  }

  createCurrentShareUrl(code: string): string {
    return `${document.location.origin}${this._router
      .createUrlTree(['join', code])
      .toString()}`;
  }

  async shareHousehold(code: string, name: string) {
    if (code && this.canUseShareApi()) {
      try {
        await (navigator as any).share({
          title: name,
          text: 'Wordt lid van mijn huishouden.',
          url: this.createCurrentShareUrl(code),
        });
      } catch (ex) {
        console.log(ex);
      }
    }
  }
}
