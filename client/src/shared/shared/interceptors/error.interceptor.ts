import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SnackbarService } from 'src/shared/snackbar/services/snackbar.service';
import {
  SnackbarType,
  SnackbarTheme,
} from 'src/shared/snackbar/types/snackbar.type';
import { Router } from '@angular/router';
import { AuthService } from 'src/shared/services/auth.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private _snackbar: SnackbarService,
    private _router: Router,
    private _auth: AuthService,
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 500) {
          if (error.error && error.error.errors) {
            error.error.errors.forEach(e => {
              this._openErrorSnackbar(e.msg);
            });
          } else {
            this._openErrorSnackbar('Er is een probleem opgetreden.');
          }
        } else if (error.status === 401) {
          this._auth.setToken(null);
          this._router.navigate(['join']);
        } else if (error.status === 0) {
          this._openErrorSnackbar('Er is een probleem opgetreden.');
        }

        return throwError(error);
      }),
    );
  }

  private _openErrorSnackbar(message: string) {
    this._snackbar.open(message, {
      duration: 5000,
      type: SnackbarType.ERROR,
      theme: SnackbarTheme.ERROR,
    });
  }
}
