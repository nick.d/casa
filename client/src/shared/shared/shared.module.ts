import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SnackbarModule } from '../snackbar/snackbar.module';
import { CustomIconsService } from './services/custom-icons.service';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { MatIconModule } from '@angular/material/icon';
import { GetStartedComponent } from './components/get-started/get-started.component';
import { CannotLoadComponent } from './components/cannot-load/cannot-load.component';

@NgModule({
  declarations: [GetStartedComponent, CannotLoadComponent],
  exports: [GetStartedComponent, CannotLoadComponent],
  imports: [CommonModule, SnackbarModule, MatIconModule],
  providers: [
    CustomIconsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    },
  ],
})
export class SharedModule {}
