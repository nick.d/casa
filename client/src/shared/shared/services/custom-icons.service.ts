import { Injectable } from "@angular/core";
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";

@Injectable()
export class CustomIconsService {
  constructor(
    private _matIconRegistry: MatIconRegistry,
    private _domSanitizer: DomSanitizer
  ) {}

  registerIcons() {
    this._matIconRegistry.addSvgIcon(
      "crown",
      this._domSanitizer.bypassSecurityTrustResourceUrl(
        "./../assets/material-icons/crown.svg"
      )
    );
    this._matIconRegistry.addSvgIcon(
      "help",
      this._domSanitizer.bypassSecurityTrustResourceUrl(
        "./../assets/material-icons/help.svg"
      )
    );
    this._matIconRegistry.addSvgIcon(
      "broom",
      this._domSanitizer.bypassSecurityTrustResourceUrl(
        "./../assets/material-icons/broom.svg"
      )
    );
  }
}
