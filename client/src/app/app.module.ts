import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import localeNlExtra from '@angular/common/locales/extra/nl-BE';
import localeNl from '@angular/common/locales/nl-BE';
import { NgModule } from '@angular/core';
import {
  BrowserModule,
  HAMMER_GESTURE_CONFIG,
  HammerGestureConfig,
} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import * as Hammer from 'hammerjs';
import { EnvServiceProvider } from 'src/shared/services/env/env.service.provider';
import { SharedModule } from 'src/shared/shared/shared.module';
import { SnackbarModule } from 'src/shared/snackbar/snackbar.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

export class HammerOverridesConfig extends HammerGestureConfig {
  overrides = {
    swipe: { direction: Hammer.DIRECTION_ALL },
  };
}
registerLocaleData(localeNl, 'nl-BE', localeNlExtra);
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SnackbarModule,
    SharedModule,
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerOverridesConfig,
    },
    EnvServiceProvider,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
