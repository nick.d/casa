import { Component, OnInit } from '@angular/core';
import {
  NavigationEnd,
  RouteConfigLoadEnd,
  RouteConfigLoadStart,
  Router,
} from '@angular/router';
import { merge, Observable } from 'rxjs';
import { debounceTime, filter, mapTo, share } from 'rxjs/operators';
import { CustomIconsService } from 'src/shared/shared/services/custom-icons.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  isModuleLoading$: Observable<boolean>;

  constructor(
    private _customIcons: CustomIconsService,
    private _router: Router,
  ) {
    _customIcons.registerIcons();
  }

  ngOnInit() {
    this._initStreams();
  }

  private _initStreams() {
    const routerEvents$ = this._router.events.pipe(share());

    this.isModuleLoading$ = merge(
      routerEvents$.pipe(
        filter(
          event =>
            event instanceof NavigationEnd ||
            event instanceof RouteConfigLoadEnd,
        ),
        debounceTime(0),
        mapTo(false),
      ),
      routerEvents$.pipe(
        filter(
          event =>
            event instanceof RouteConfigLoadStart ||
            event instanceof NavigationEnd ||
            event instanceof RouteConfigLoadEnd,
        ),
        debounceTime(500),
        filter(event => event instanceof RouteConfigLoadStart),
        mapTo(true),
      ),
    );
  }
}
