import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/shared/guards/auth.guard';
import { HasHousholdGuard } from 'src/shared/guards/has-houshold.guard';

const routes: Routes = [
  {
    path: 'join',
    canLoad: [HasHousholdGuard],
    loadChildren: () =>
      import('./modules/join/join.module').then(m => m.JoinModule),
  },
  {
    path: 'household',
    canLoad: [AuthGuard],
    loadChildren: () =>
      import('./modules/household/household.module').then(
        m => m.HouseholdModule,
      ),
  },
  { path: '', redirectTo: 'join', pathMatch: 'full' },
  { path: '**', redirectTo: 'join', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
