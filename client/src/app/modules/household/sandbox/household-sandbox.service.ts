import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HouseholdInfo } from 'src/shared/types/household/household-info';
import { HouseholdService } from 'src/shared/services/household.service';

@Injectable({
  providedIn: 'root',
})
export class HouseholdSandboxService {
  constructor(private _householdService: HouseholdService) {}

  getHouseholdInfo(): Observable<HouseholdInfo> {
    return this._householdService.info();
  }
}
