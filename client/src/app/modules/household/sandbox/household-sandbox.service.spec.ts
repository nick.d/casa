import { TestBed } from '@angular/core/testing';

import { HouseholdSandboxService } from './household-sandbox.service';

describe('HouseholdSandboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HouseholdSandboxService = TestBed.get(HouseholdSandboxService);
    expect(service).toBeTruthy();
  });
});
