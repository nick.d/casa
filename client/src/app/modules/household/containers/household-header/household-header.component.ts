import {
  animate,
  state,
  style,
  transition,
  trigger,
  group,
  query,
  animateChild,
} from '@angular/animations';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { merge, Observable, of } from 'rxjs';
import {
  debounceTime,
  filter,
  first,
  map,
  mapTo,
  skip,
  startWith,
  delay,
  catchError,
} from 'rxjs/operators';
import { HouseholdSandboxService } from '../../sandbox/household-sandbox.service';
import { HouseholdInfo } from 'src/shared/types/household/household-info';

@Component({
  selector: 'app-household-header',
  templateUrl: './household-header.component.html',
  styleUrls: ['./household-header.component.scss'],
  animations: [
    trigger('collapseHeader', [
      state(
        'open',
        style({
          transform: 'translateY(100%)',
        }),
      ),
      state(
        'collapsed',
        style({
          transform: 'translateY(4.2rem)',
        }),
      ),
      transition('open <=> collapsed', [
        group([
          query('@navVisibility', animateChild()),
          query('@breadcrumbVisibility', animateChild()),
          animate('0.25s ease'),
        ]),
      ]),
    ]),
    trigger('backdropCollapse', [
      state(
        'hidden',
        style({
          opacity: 0,
          transform: 'translateY(-100%)',
        }),
      ),
      state(
        'visible',
        style({
          opacity: 1,
          transform: 'translateY(0)',
        }),
      ),
      transition('hidden => visible', [
        group([
          animate(
            '0.25s ease',
            style({
              opacity: 1,
            }),
          ),
          animate(
            '0s',
            style({
              transform: 'translateY(0)',
            }),
          ),
        ]),
      ]),
      transition('visible => hidden', [
        group([
          animate(
            '0.25s ease',
            style({
              opacity: 0,
            }),
          ),
          animate(
            '0.25s 0.25s',
            style({
              transform: 'translateY(-100%)',
            }),
          ),
        ]),
      ]),
    ]),
    // trigger("backdropOpacity", [
    //   state("hidden", style({ opacity: 0 })),
    //   state("visible", style({ opacity: 1 })),
    //   transition("hidden <=> visible", [animate("0.25s ease")])
    // ]),
    // trigger("backdropPosition", [
    //   state("hidden", style({ transform: "translateY(-100%)" })),
    //   state("visible", style({ transform: "translateY(0)" })),
    //   transition("visible => hidden", [animate("0.25s 0.25s")])
    // ]),
    trigger('navVisibility', [
      state(
        'hidden',
        style({
          transform: 'translateY(-2rem)',
          opacity: 0,
        }),
      ),
      state(
        'visible',
        style({
          transform: 'translateY(0)',
          opacity: 1,
        }),
      ),
      transition('visible => hidden', [animate('0.25s ease')]),
      transition('hidden => visible', [animate('0.5s 0.075s ease')]),
    ]),
    trigger('breadcrumbVisibility', [
      state(
        'hidden',
        style({
          transform: 'translateY(2rem)',
          opacity: 0,
        }),
      ),
      state(
        'visible',
        style({
          transform: 'translateY(0)',
          opacity: 1,
        }),
      ),
      transition('visible => hidden', [animate('0.25s ease')]),
      transition('visible <=> hidden', [animate('0.5s 0.075s ease')]),
    ]),
  ],
})
export class HouseholdHeaderComponent implements OnInit {
  isOpen = false;
  transitionClass: string = null;
  headerThemeClass$: Observable<string>;
  headerTransitionClass$: Observable<string>;
  breadcrumb$: Observable<string[]>;
  householdInfo$: Observable<HouseholdInfo>;
  errorLoadinghouseholdInfo = false;

  constructor(
    private _router: Router,
    private _location: Location,
    private _householdSandbox: HouseholdSandboxService,
  ) {}

  ngOnInit() {
    this._initRouteEvents();
    this._initStreams();
  }

  private _initStreams() {
    this.householdInfo$ = this._householdSandbox.getHouseholdInfo().pipe(
      catchError(_ => {
        this.errorLoadinghouseholdInfo = true;

        return of(null);
      }),
    );
  }

  private _initRouteEvents() {
    const routeChanged$ = this._router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map((event: NavigationEnd) => event.urlAfterRedirects),
    );

    this.headerThemeClass$ = merge(
      routeChanged$.pipe(skip(1), debounceTime(600)),
      routeChanged$.pipe(first()),
    ).pipe(
      map(route => `-${this._getThemeClass(route)}`),
      startWith(`-${this._getThemeClass(this._location.path(false))}`),
    );

    this.headerTransitionClass$ = merge(
      routeChanged$.pipe(mapTo(null)),
      routeChanged$.pipe(
        map(route => `-to-${this._getThemeClass(route)}`),
        debounceTime(0),
      ),
    );

    this.breadcrumb$ = routeChanged$.pipe(
      map(this._getBreadcrumbData),
      startWith(this._getBreadcrumbData(this._location.path(false))),
    );
  }

  private _getBreadcrumbData(route: string) {
    if (route.includes('/household/overview')) {
      return ['visibility', 'Overzicht'];
    } else if (route.includes('/household/eat-calendar')) {
      return ['restaurant', 'Eetkalender'];
    } else if (route.includes('/household/productivity')) {
      return ['timer', 'Productiviteit'];
    } else if (route.includes('/household/settings')) {
      return ['settings', 'Instellingen'];
    }
  }

  private _getThemeClass(route: string) {
    if (route.includes('/household/overview')) {
      return 'blue';
    } else if (route.includes('/household/eat-calendar')) {
      return 'purple';
    } else if (route.includes('/household/productivity')) {
      return 'orange';
    } else if (route.includes('/household/settings')) {
      return 'green';
    } else {
      return 'blue';
    }
  }

  openNavigation() {
    this.isOpen = true;
  }

  closeNavigation() {
    this.isOpen = false;
  }

  onSwipeDown(event: any) {
    this.openNavigation();
  }

  onSwipeUp(event: any) {
    this.closeNavigation();
  }
}
