import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { HouseholdHeaderComponent } from "./household-header.component";

describe("HouseholdHeaderComponent", () => {
  let component: HouseholdHeaderComponent;
  let fixture: ComponentFixture<HouseholdHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HouseholdHeaderComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseholdHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
