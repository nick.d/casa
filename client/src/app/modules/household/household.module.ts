import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HouseholdRoutingModule } from "./household-routing.module";
import { HouseholdComponent } from "./components/household/household.component";
import { HouseholdHeaderComponent } from "./containers/household-header/household-header.component";
import { MatIconModule } from "@angular/material/icon";

@NgModule({
  declarations: [HouseholdComponent, HouseholdHeaderComponent],
  imports: [CommonModule, HouseholdRoutingModule, MatIconModule]
})
export class HouseholdModule {}
