import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HouseholdComponent } from "./components/household/household.component";

const routes: Routes = [
  {
    path: "",
    component: HouseholdComponent,
    children: [
      {
        path: "settings",
        loadChildren: () =>
          import("./modules/settings/settings.module").then(
            m => m.SettingsModule
          )
      },
      {
        path: "overview",
        loadChildren: () =>
          import("./modules/overview/overview.module").then(
            m => m.OverviewModule
          )
      },
      {
        path: "eat-calendar",
        loadChildren: () =>
          import("./modules/eat-calendar/eat-calendar.module").then(
            m => m.EatCalendarModule
          )
      },
      {
        path: "productivity",
        loadChildren: () =>
          import("./modules/productivity/productivity.module").then(
            m => m.ProductivityModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdRoutingModule {}
