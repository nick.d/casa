import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingsComponent } from './containers/settings/settings.component';
import { AboutContainerComponent } from './containers/about-container/about-container.component';
import { UserContainerComponent } from './containers/user-container/user-container.component';
import { HouseholdContainerComponent } from './containers/household-container/household-container.component';
import { SignOutContainerComponent } from './containers/sign-out-container/sign-out-container.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
  },
  {
    path: 'user',
    component: UserContainerComponent,
  },
  {
    path: 'household',
    component: HouseholdContainerComponent,
  },
  {
    path: 'sign-out',
    component: SignOutContainerComponent,
  },
  {
    path: 'about',
    component: AboutContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsRoutingModule {}
