import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HouseholdContainerComponent } from './household-container.component';

describe('HouseholdContainerComponent', () => {
  let component: HouseholdContainerComponent;
  let fixture: ComponentFixture<HouseholdContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HouseholdContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseholdContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
