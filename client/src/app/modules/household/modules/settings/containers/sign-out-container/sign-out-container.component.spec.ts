import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SignOutContainerComponent } from './sign-out-container.component';

describe('SignOutContainerComponent', () => {
  let component: SignOutContainerComponent;
  let fixture: ComponentFixture<SignOutContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SignOutContainerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignOutContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
