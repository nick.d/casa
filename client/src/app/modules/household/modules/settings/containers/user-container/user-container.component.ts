import { Component, OnInit } from '@angular/core';
import { SettingsSandboxService } from '../../sandbox/settings-sandbox.service';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-user-container',
  templateUrl: './user-container.component.html',
  styleUrls: ['./user-container.component.scss'],
})
export class UserContainerComponent implements OnInit {
  userInfo$: Observable<any>;
  userInfoError = false;
  private _trigger$ = new BehaviorSubject<void>(null);

  constructor(private _settingsSandbox: SettingsSandboxService) {}

  ngOnInit() {
    this._initStreams();
  }

  private _initStreams() {
    this.userInfo$ = this._trigger$.pipe(
      switchMap(() =>
        this._settingsSandbox.getUser().pipe(
          catchError(() => {
            this.userInfoError = true;

            return of(null);
          }),
        ),
      ),
    );
  }

  onRefreshData() {
    this.userInfoError = false;

    this._trigger$.next();
  }
}
