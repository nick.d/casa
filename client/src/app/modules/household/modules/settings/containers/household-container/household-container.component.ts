import { Component, OnInit } from '@angular/core';
import { SettingsSandboxService } from '../../sandbox/settings-sandbox.service';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { delay, switchMap, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-household-container',
  templateUrl: './household-container.component.html',
  styleUrls: ['./household-container.component.scss'],
})
export class HouseholdContainerComponent implements OnInit {
  householdInfo$: Observable<any>;
  householdInfoError = false;
  private _trigger$ = new BehaviorSubject<void>(null);

  constructor(private _settingsSandbox: SettingsSandboxService) {}

  ngOnInit() {
    this._initStreams();
  }

  private _initStreams() {
    this.householdInfo$ = this._trigger$.pipe(
      switchMap(() =>
        this._settingsSandbox.getHousehold().pipe(
          catchError(() => {
            this.householdInfoError = true;

            return of(null);
          }),
        ),
      ),
    );
  }

  onRefreshData() {
    this.householdInfoError = false;

    this._trigger$.next();
  }
}
