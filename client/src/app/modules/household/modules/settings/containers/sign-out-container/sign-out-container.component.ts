import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/shared/services/auth.service';
import { Router } from '@angular/router';
import { SnackbarService } from 'src/shared/snackbar/services/snackbar.service';
import {
  SnackbarType,
  SnackbarTheme,
} from 'src/shared/snackbar/types/snackbar.type';
import { DialogService } from 'src/shared/dialog/services/dialog.service';
import { take } from 'rxjs/operators';
import { ConfirmDialogParameters } from 'src/shared/dialog/types/confirm-dialog-paramaters';
import { ConfirmDialogComponent } from 'src/shared/dialog/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-sign-out',
  templateUrl: './sign-out-container.component.html',
  styleUrls: ['./sign-out-container.component.scss'],
})
export class SignOutContainerComponent implements OnInit {
  constructor(
    private _auth: AuthService,
    private _router: Router,
    private _snackbar: SnackbarService,
    private _dialog: DialogService,
  ) {}

  ngOnInit() {}

  signOut() {
    const dialogRef = this._dialog.open<ConfirmDialogParameters>({
      contentComponent: ConfirmDialogComponent,
      config: {
        theme: 'green',
        type: 'normal',
        closeOnBackdropClick: true,
      },
      data: {
        title: 'Uitloggen',
        message: 'Je wordt uitgelogd.',
        accept: 'OK',
        decline: 'Annuleer',
      },
    });

    dialogRef.afterClosed$.pipe(take(1)).subscribe(closeEvent => {
      if (closeEvent.reason === 'accepted') {
        this._auth.setToken();
        this._router.navigate(['join']);
      }
    });
  }

  onLeavePermanently() {
    this._snackbar.open('Werkt nog niet :(', {
      duration: 5000,
      type: SnackbarType.WARNING,
      theme: SnackbarTheme.WARNING,
    });
  }
}
