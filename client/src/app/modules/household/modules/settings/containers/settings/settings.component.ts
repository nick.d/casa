import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, share, switchMap } from 'rxjs/operators';
import { HouseholdSandboxService } from 'src/app/modules/household/sandbox/household-sandbox.service';
import { HouseholdInfo } from 'src/shared/types/household/household-info';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  householdInfo$: Observable<HouseholdInfo>;
  householdInfoError = false;
  private _trigger$ = new BehaviorSubject<void>(null);

  constructor(private _householdSandbox: HouseholdSandboxService) {}

  ngOnInit() {
    this._initStreams();
  }

  private _initStreams() {
    this.householdInfo$ = this._trigger$
      .pipe(
        switchMap(() =>
          this._householdSandbox.getHouseholdInfo().pipe(
            catchError(() => {
              this.householdInfoError = true;

              return of(null);
            }),
          ),
        ),
      )
      .pipe(share());
  }

  onLoadInfoAgain() {
    this._trigger$.next();
    this.householdInfoError = false;
  }
}
