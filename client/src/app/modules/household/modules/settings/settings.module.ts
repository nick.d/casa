import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './containers/settings/settings.component';
import { SharedModule } from 'src/shared/shared/shared.module';
import { MatIconModule } from '@angular/material/icon';
import { AboutContainerComponent } from './containers/about-container/about-container.component';
import { UserContainerComponent } from './containers/user-container/user-container.component';
import { HouseholdContainerComponent } from './containers/household-container/household-container.component';
import { SignOutContainerComponent } from './containers/sign-out-container/sign-out-container.component';
import { DialogModule } from 'src/shared/dialog/dialog.module';

@NgModule({
  declarations: [
    SettingsComponent,
    AboutContainerComponent,
    SignOutContainerComponent,
    UserContainerComponent,
    HouseholdContainerComponent,
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    SharedModule,
    MatIconModule,
    DialogModule,
  ],
})
export class SettingsModule {}
