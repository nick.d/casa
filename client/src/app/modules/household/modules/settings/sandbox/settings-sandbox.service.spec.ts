import { TestBed } from '@angular/core/testing';

import { SettingsSandboxService } from './settings-sandbox.service';

describe('SettingsSandboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SettingsSandboxService = TestBed.get(SettingsSandboxService);
    expect(service).toBeTruthy();
  });
});
