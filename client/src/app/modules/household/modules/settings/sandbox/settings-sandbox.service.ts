import { Injectable } from '@angular/core';
import { HouseholdService } from 'src/shared/services/household.service';
import { HouseholdInfo } from 'src/shared/types/household/household-info';
import { Observable } from 'rxjs';
import { UserService } from 'src/shared/services/user.service';

@Injectable({
  providedIn: 'root',
})
export class SettingsSandboxService {
  constructor(
    private _householdService: HouseholdService,
    private _userService: UserService,
  ) {}

  getHouseholdInfo(): Observable<HouseholdInfo> {
    return this._householdService.info();
  }

  getUser(): Observable<any> {
    return this._userService.getUser();
  }

  getHousehold(): Observable<any> {
    return this._householdService.getHousehold();
  }
}
