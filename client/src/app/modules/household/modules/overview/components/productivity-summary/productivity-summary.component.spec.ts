import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductivitySummaryComponent } from './productivity-summary.component';

describe('ProductivitySummaryComponent', () => {
  let component: ProductivitySummaryComponent;
  let fixture: ComponentFixture<ProductivitySummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductivitySummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductivitySummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
