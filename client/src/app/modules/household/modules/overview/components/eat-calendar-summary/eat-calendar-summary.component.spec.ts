import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EatCalendarSummaryComponent } from './eat-calendar-summary.component';

describe('EatCalendarSummaryComponent', () => {
  let component: EatCalendarSummaryComponent;
  let fixture: ComponentFixture<EatCalendarSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EatCalendarSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EatCalendarSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
