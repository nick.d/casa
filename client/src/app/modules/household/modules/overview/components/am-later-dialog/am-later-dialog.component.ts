import { Component, OnInit, OnDestroy } from '@angular/core';
import { DialogRef } from 'src/shared/dialog/types/dialog-ref';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { CustomValidators } from 'src/shared/validators/custom.validator';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-am-later-dialog',
  templateUrl: './am-later-dialog.component.html',
  styleUrls: ['./am-later-dialog.component.scss'],
})
export class AmLaterDialogComponent implements OnInit, OnDestroy {
  theme: string;
  amLaterForm: FormGroup;
  private _destroy$ = new Subject<void>();

  constructor(private _dialogRef: DialogRef, private _fb: FormBuilder) {
    this.theme = _dialogRef.config.theme;
  }

  ngOnInit() {
    this._initForm();
    this._initStreams();
  }

  ngOnDestroy() {
    this._destroy$.next();
    this._destroy$.complete();
  }

  private _initStreams() {
    this.dontKnowLaterControl.valueChanges
      .pipe(takeUntil(this._destroy$))
      .subscribe(_ => {
        this.laterHoursControl.updateValueAndValidity();
      });
  }

  private _initForm() {
    this.amLaterForm = this._fb.group({
      dontKnowLater: [],
      laterHours: [
        2,
        [
          Validators.min(0),
          Validators.max(12),
          CustomValidators.requiredIf(() => !this.dontKnowLaterControl.value),
        ],
      ],
    });
  }

  onSubmitAmLater(form: FormGroup) {
    if (form.valid) {
      if (!this.dontKnowLaterControl.value) {
        this._dialogRef.close('accepted', {
          laterHours: this.laterHoursControl.value,
        });
      } else {
        this._dialogRef.close('accepted');
      }
    }
  }

  onDecreaseHours() {
    if (+this.laterHoursControl.value >= 0.5) {
      this.laterHoursControl.setValue(
        Math.min(12, +this.laterHoursControl.value - 0.5),
      );
    }
  }

  onIncreaseHours() {
    if (+this.laterHoursControl.value <= 11.5) {
      this.laterHoursControl.setValue(
        Math.max(0, +this.laterHoursControl.value + 0.5),
      );
    }
  }

  onDecline() {
    this._dialogRef.close('declined');
  }

  get dontKnowLaterControl(): FormControl {
    return this.amLaterForm.controls['dontKnowLater'] as FormControl;
  }

  get laterHoursControl(): FormControl {
    return this.amLaterForm.controls['laterHours'] as FormControl;
  }
}
