import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-eat-calendar-summary",
  templateUrl: "./eat-calendar-summary.component.html",
  styleUrls: ["./eat-calendar-summary.component.scss"]
})
export class EatCalendarSummaryComponent implements OnInit {
  @Input() isLoading: boolean;
  @Output() wontEat = new EventEmitter<void>();
  @Output() amLater = new EventEmitter<void>();

  constructor() {}

  ngOnInit() {}

  onWontEat() {
    this.wontEat.emit();
  }

  onAmLater() {
    this.amLater.emit();
  }
}
