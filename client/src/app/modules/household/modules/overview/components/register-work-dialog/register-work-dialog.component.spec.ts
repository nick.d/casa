import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterWorkDialogComponent } from './register-work-dialog.component';

describe('RegisterWorkDialogComponent', () => {
  let component: RegisterWorkDialogComponent;
  let fixture: ComponentFixture<RegisterWorkDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterWorkDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterWorkDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
