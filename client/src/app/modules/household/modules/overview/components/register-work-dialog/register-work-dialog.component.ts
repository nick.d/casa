import { Component, OnInit, Input } from '@angular/core';
import { DialogRef } from 'src/shared/dialog/types/dialog-ref';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-register-work-dialog',
  templateUrl: './register-work-dialog.component.html',
  styleUrls: ['./register-work-dialog.component.scss'],
})
export class RegisterWorkDialogComponent implements OnInit {
  theme: string;
  registerWorkForm: FormGroup;

  constructor(private _dialogRef: DialogRef, private _fb: FormBuilder) {
    this.theme = _dialogRef.config.theme;
  }

  ngOnInit() {
    this._initForm();
    this._formEvents();
  }

  private _formEvents() {
    this.minutesWorkedControl.valueChanges
      .pipe(
        filter(_ => !this.descriptionControl.dirty),
        filter(value => !!value),
      )
      .subscribe(value => {
        this.descriptionControl.patchValue(
          `${value} ${value === 1 ? 'minuut' : 'minuten'} gewerkt`,
        );
      });
  }

  private _initForm() {
    this.registerWorkForm = this._fb.group({
      minutesWorked: [
        30,
        [Validators.required, Validators.min(1), Validators.max(300)],
      ],
      description: [
        '30 minuten gewerkt',
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(64),
        ],
      ],
    });
  }

  onRegisterWork(form: FormGroup) {
    if (form.valid) {
      this._dialogRef.close('accepted', {
        minutesWorked: +this.minutesWorkedControl.value,
        description: this.descriptionControl.value,
      });
    }
  }

  onDecreaseMinutes() {
    if (+this.minutesWorkedControl.value >= 1) {
      this.minutesWorkedControl.setValue(
        Math.min(300, Math.max(1, +this.minutesWorkedControl.value - 10)),
      );
    }
  }

  onIncreaseMinutes() {
    if (+this.minutesWorkedControl.value <= 299) {
      this.minutesWorkedControl.setValue(
        Math.max(1, Math.min(300, +this.minutesWorkedControl.value + 10)),
      );
    }
  }

  onDecline() {
    this._dialogRef.close('declined');
  }

  get minutesWorkedControl(): FormControl {
    return this.registerWorkForm.controls['minutesWorked'] as FormControl;
  }

  get descriptionControl(): FormControl {
    return this.registerWorkForm.controls['description'] as FormControl;
  }
}
