import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmLaterDialogComponent } from './am-later-dialog.component';

describe('AmLaterDialogComponent', () => {
  let component: AmLaterDialogComponent;
  let fixture: ComponentFixture<AmLaterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmLaterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmLaterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
