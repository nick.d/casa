import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { WorkSummary } from 'src/shared/types/productivity/work-summary';

@Component({
  selector: 'app-productivity-summary',
  templateUrl: './productivity-summary.component.html',
  styleUrls: ['./productivity-summary.component.scss'],
})
export class ProductivitySummaryComponent implements OnInit {
  isLoading = false;
  @Input() workSummary: WorkSummary[];
  @Input() loadingWorkError: boolean;
  @Input() registerWorkErrors: string[];
  @Input() isRegisteringWork: boolean;
  @Input() isRegisteringThirtyMinutes: boolean;
  @Output() registerWork = new EventEmitter<void>();
  @Output() thirtyMinutes = new EventEmitter<void>();
  @Output() loadWorkAgain = new EventEmitter<void>();

  constructor() {}

  ngOnInit() {}

  closeError(error: string) {
    this.registerWorkErrors.splice(this.registerWorkErrors.indexOf(error), 1);
  }

  onLoadWorkAgain() {
    this.loadWorkAgain.next();
  }

  onRegisterWork() {
    this.registerWork.emit();
  }

  onThirtyMinutes() {
    this.thirtyMinutes.emit();
  }
}
