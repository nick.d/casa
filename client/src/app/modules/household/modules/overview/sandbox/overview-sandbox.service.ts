import { Injectable } from '@angular/core';
import { ProductivityService } from 'src/shared/services/productivity.service';
import { RegisterWorkParams } from 'src/shared/types/productivity/register-work-params';
import { Observable } from 'rxjs';
import { WorkSummary } from 'src/shared/types/productivity/work-summary';

@Injectable({
  providedIn: 'root',
})
export class OverviewSandboxService {
  constructor(private _productivityService: ProductivityService) {}

  registerWork(params: RegisterWorkParams): Observable<boolean> {
    return this._productivityService.register(params);
  }

  getWeeklyWorkOverview(): Observable<WorkSummary[]> {
    return this._productivityService.weeklyOverview();
  }
}
