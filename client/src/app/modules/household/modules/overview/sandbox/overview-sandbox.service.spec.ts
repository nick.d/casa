import { TestBed } from '@angular/core/testing';

import { OverviewSandboxService } from './overview-sandbox.service';

describe('OverviewSandboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OverviewSandboxService = TestBed.get(OverviewSandboxService);
    expect(service).toBeTruthy();
  });
});
