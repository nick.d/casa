import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, merge, Observable, of } from 'rxjs';
import {
  catchError,
  mapTo,
  materialize,
  share,
  switchMap,
  take,
} from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/shared/dialog/components/confirm-dialog/confirm-dialog.component';
import { DialogService } from 'src/shared/dialog/services/dialog.service';
import { ConfirmDialogParameters } from 'src/shared/dialog/types/confirm-dialog-paramaters';
import { SnackbarService } from 'src/shared/snackbar/services/snackbar.service';
import {
  SnackbarTheme,
  SnackbarType,
} from 'src/shared/snackbar/types/snackbar.type';
import { WorkSummary } from 'src/shared/types/productivity/work-summary';

import { AmLaterDialogComponent } from '../../components/am-later-dialog/am-later-dialog.component';
import { RegisterWorkDialogComponent } from '../../components/register-work-dialog/register-work-dialog.component';
import { OverviewSandboxService } from '../../sandbox/overview-sandbox.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
})
export class OverviewComponent implements OnInit {
  weeklyWorkOverview$: Observable<WorkSummary[]>;
  weeklyWorkOverviewError$: Observable<boolean>;
  registerWorkErrors: string[];
  isRegisteringWork = false;
  isRegisteringThirtyMinutes = false;
  private _triggerRefresh$ = new BehaviorSubject<void>(null);

  constructor(
    private _dialog: DialogService,
    private _snackbar: SnackbarService,
    private _overviewSandbox: OverviewSandboxService,
  ) {}

  ngOnInit() {
    this._initStreams();
  }

  private _initStreams() {
    this.weeklyWorkOverview$ = this._triggerRefresh$
      .pipe(
        switchMap(() =>
          this._overviewSandbox.getWeeklyWorkOverview().pipe(
            catchError(e => {
              return of(null);
            }),
          ),
        ),
      )
      .pipe(share());

    this.weeklyWorkOverviewError$ = merge(
      this.weeklyWorkOverview$.pipe(materialize(), mapTo(true)),
      this._triggerRefresh$.pipe(mapTo(false)),
    );
  }

  onLoadWorkAgain() {
    this._triggerRefresh$.next();
  }

  onWontEat() {
    const dialogRef = this._dialog.open<ConfirmDialogParameters>({
      contentComponent: ConfirmDialogComponent,
      config: {
        theme: 'purple',
        type: 'normal',
        closeOnBackdropClick: true,
      },
      data: {
        title: 'Ik eet niet mee',
        message: 'Je eet niet mee vandaag?',
        accept: 'OK',
        decline: 'Annuleer',
      },
    });

    dialogRef.afterClosed$.pipe(take(1)).subscribe(closeEvent => {
      if (closeEvent.reason === 'accepted') {
        this._snackbar.open('Je eet niet mee vandaag.', {
          duration: 5000,
          type: SnackbarType.SUCCESS,
          theme: SnackbarTheme.SUCCESS,
        });
      }
    });
  }

  onAmLater() {
    const dialogRef = this._dialog.open({
      contentComponent: AmLaterDialogComponent,
      config: {
        theme: 'purple',
        type: 'normal',
        closeOnBackdropClick: true,
      },
    });

    dialogRef.afterClosed$.pipe(take(1)).subscribe(closeEvent => {
      if (closeEvent.reason === 'accepted') {
        if (
          (closeEvent.data && closeEvent.data.laterHours) ||
          (closeEvent.data && closeEvent.data.laterHours === 0)
        ) {
          this._snackbar.open(
            `Je bent thuis over ${closeEvent.data.laterHours} ${
              closeEvent.data.laterHours === 1 ? 'uur' : 'uren'
            }.`,
            {
              duration: 5000,
              type: SnackbarType.SUCCESS,
              theme: SnackbarTheme.SUCCESS,
            },
          );

          return;
        }

        this._snackbar.open('Je zal later eten.', {
          duration: 5000,
          type: SnackbarType.SUCCESS,
          theme: SnackbarTheme.SUCCESS,
        });
      }
    });
  }

  onRegisterWork() {
    const dialogRef = this._dialog.open({
      contentComponent: RegisterWorkDialogComponent,
      config: {
        theme: 'orange',
        type: 'normal',
        closeOnBackdropClick: true,
      },
    });

    dialogRef.afterClosed$.pipe(take(1)).subscribe(closeEvent => {
      if (closeEvent.reason === 'accepted') {
        if (
          closeEvent.data &&
          closeEvent.data.minutesWorked &&
          closeEvent.data.description
        ) {
          this.isRegisteringWork = true;
          this._overviewSandbox
            .registerWork({
              description: closeEvent.data.description,
              durationInMinutes: closeEvent.data.minutesWorked,
            })
            .pipe(catchError(e => this._catchRegisterWorkError(e)))
            .subscribe(_ => {
              this._triggerRefresh$.next();
              this.isRegisteringWork = false;

              this._snackbar.open(
                `Je hebt ${closeEvent.data.minutesWorked} ${
                  closeEvent.data.minutesWorked === 1 ? 'minuut' : 'minuten'
                } gewerkt.`,
                {
                  duration: 5000,
                  type: SnackbarType.SUCCESS,
                  theme: SnackbarTheme.SUCCESS,
                },
              );
            });
        }
      }
    });
  }

  private _catchRegisterWorkError(e) {
    this.isRegisteringWork = false;

    if (e.status !== 500 && e.error && e.error.errors) {
      this.registerWorkErrors = e.error.errors.map(error => error.msg);
    }

    return of<boolean>();
  }

  private _catchRegisterThrityMinutesError(e) {
    this.isRegisteringThirtyMinutes = false;

    if (e.status !== 500 && e.error && e.error.errors) {
      this.registerWorkErrors = e.error.errors.map(error => error.msg);
    }

    return of<boolean>();
  }

  onThirtyMinutes() {
    const dialogRef = this._dialog.open<ConfirmDialogParameters>({
      contentComponent: ConfirmDialogComponent,
      config: {
        theme: 'orange',
        type: 'normal',
        closeOnBackdropClick: true,
      },
      data: {
        title: '30 minuten gewerkt',
        message: 'Je hebt 30 minuten gewerkt?',
        accept: 'OK',
        decline: 'Annuleer',
      },
    });

    dialogRef.afterClosed$.pipe(take(1)).subscribe(closeEvent => {
      if (closeEvent.reason === 'accepted') {
        this.isRegisteringThirtyMinutes = true;
        this._overviewSandbox
          .registerWork({
            description: '30 minuten gewerkt',
            durationInMinutes: 30,
          })
          .pipe(catchError(e => this._catchRegisterThrityMinutesError(e)))
          .subscribe(_ => {
            this._triggerRefresh$.next();
            this.isRegisteringThirtyMinutes = false;

            this._snackbar.open('Je hebt 30 minuten gewerkt.', {
              duration: 5000,
              type: SnackbarType.SUCCESS,
              theme: SnackbarTheme.SUCCESS,
            });
          });
      }
    });
  }
}
