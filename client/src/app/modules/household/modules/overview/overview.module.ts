import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';

import { OverviewComponent } from './containers/overview/overview.component';
import { OverviewRoutingModule } from './overview-routing.module';
import { EatCalendarSummaryComponent } from './components/eat-calendar-summary/eat-calendar-summary.component';
import { ProductivitySummaryComponent } from './components/productivity-summary/productivity-summary.component';
import { AmLaterDialogComponent } from './components/am-later-dialog/am-later-dialog.component';
import { DialogModule } from 'src/shared/dialog/dialog.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterWorkDialogComponent } from './components/register-work-dialog/register-work-dialog.component';
import { SharedModule } from 'src/shared/shared/shared.module';

@NgModule({
  declarations: [
    OverviewComponent,
    EatCalendarSummaryComponent,
    ProductivitySummaryComponent,
    AmLaterDialogComponent,
    RegisterWorkDialogComponent,
  ],
  imports: [
    CommonModule,
    OverviewRoutingModule,
    MatIconModule,
    DialogModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  entryComponents: [AmLaterDialogComponent, RegisterWorkDialogComponent],
})
export class OverviewModule {}
