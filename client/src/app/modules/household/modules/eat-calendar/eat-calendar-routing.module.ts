import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { EatCalendarComponent } from "./containers/eat-calendar/eat-calendar.component";

const routes: Routes = [
  {
    path: "",
    component: EatCalendarComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EatCalendarRoutingModule {}
