import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { EatCalendarRoutingModule } from "./eat-calendar-routing.module";
import { EatCalendarComponent } from "./containers/eat-calendar/eat-calendar.component";

@NgModule({
  declarations: [EatCalendarComponent],
  imports: [CommonModule, EatCalendarRoutingModule]
})
export class EatCalendarModule {}
