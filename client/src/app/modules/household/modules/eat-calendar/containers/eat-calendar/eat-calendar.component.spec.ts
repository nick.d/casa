import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EatCalendarComponent } from './eat-calendar.component';

describe('EatCalendarComponent', () => {
  let component: EatCalendarComponent;
  let fixture: ComponentFixture<EatCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EatCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EatCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
