import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductivityRoutingModule } from './productivity-routing.module';
import { ProductivityComponent } from './containers/productivity/productivity.component';
import { HistoryOverviewComponent } from './containers/history-overview/history-overview.component';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ProductivityComponent, HistoryOverviewComponent],
  imports: [
    CommonModule,
    ProductivityRoutingModule,
    MatIconModule,
    FormsModule,
  ],
})
export class ProductivityModule {}
