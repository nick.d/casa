import { TestBed } from '@angular/core/testing';

import { ProductivitySandboxService } from './productivity-sandbox.service';

describe('ProductivitySandboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductivitySandboxService = TestBed.get(ProductivitySandboxService);
    expect(service).toBeTruthy();
  });
});
