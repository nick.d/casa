import { Injectable } from '@angular/core';
import { ProductivityService } from 'src/shared/services/productivity.service';
import { Observable } from 'rxjs';
import { WorkSummary } from 'src/shared/types/productivity/work-summary';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ProductivitySandboxService {
  constructor(private _productivityService: ProductivityService) {}

  getWorkOverview(
    startDate: string,
    endDate: string,
  ): Observable<WorkSummary[]> {
    let params: HttpParams;

    if (startDate && endDate) {
      params = new HttpParams()
        .set('startDate', startDate)
        .set('endDate', endDate);
    }
    return this._productivityService.overview(params);
  }
}
