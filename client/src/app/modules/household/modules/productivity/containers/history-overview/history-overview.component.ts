import { formatDate } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as dateFns from 'date-fns';
import { Observable, Observer, of, ReplaySubject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { WorkSummary } from 'src/shared/types/productivity/work-summary';

import { ProductivitySandboxService } from '../../sandbox/productivity-sandbox.service';

@Component({
  selector: 'app-history-overview',
  templateUrl: './history-overview.component.html',
  styleUrls: ['./history-overview.component.scss'],
})
export class HistoryOverviewComponent implements OnInit {
  periodBarIsSticky$: Observable<boolean>;
  workOverview$: Observable<WorkSummary[]>;
  private _setOverviewFitler$ = new ReplaySubject<{
    startDate: string;
    endDate: string;
  }>();
  startDate: Date;
  endDate: Date;
  filterType: 'WEEK' | 'MONTH' | 'YEAR' | 'ALWAYS' = 'MONTH';
  get periodText(): string {
    const endDate = dateFns.subDays(this.endDate, 1);

    if (this.filterType === 'WEEK') {
      const referenceDateFirstOfWeek = dateFns.startOfWeek(new Date(), {
        weekStartsOn: 1,
      });

      if (
        dateFns.compareAsc(
          this._formatDateForTransport(this.startDate),
          this._formatDateForTransport(referenceDateFirstOfWeek),
        ) === 0
      ) {
        return 'deze week';
      }

      return `${formatDate(
        this.startDate,
        endDate.getMonth() === this.startDate.getMonth() ? 'd' : 'd MMMM',
        'nl-BE',
      )} - ${formatDate(endDate, 'd MMMM', 'nl-BE')}`;
    } else if (this.filterType === 'MONTH') {
      return formatDate(this.startDate, 'MMMM yyyy', 'nl-BE');
    } else if (this.filterType === 'YEAR') {
      return formatDate(this.startDate, 'yyyy', 'nl-BE');
    } else if (this.filterType === 'ALWAYS') {
      return 'altijd';
    }

    return '';
  }

  @ViewChild('periodBar', { static: true }) periodBar: ElementRef;

  constructor(private _productivitySandbox: ProductivitySandboxService) {}

  ngOnInit() {
    this._initStreams();
    if (this.filterType === 'WEEK') {
      this.startDate = dateFns.startOfWeek(new Date(), { weekStartsOn: 1 });
      this.endDate = dateFns.addDays(
        dateFns.endOfWeek(new Date(), { weekStartsOn: 1 }),
        1,
      );
    } else if (this.filterType === 'MONTH') {
      this.startDate = dateFns.startOfMonth(new Date());
      this.endDate = dateFns.addDays(dateFns.endOfMonth(new Date()), 1);
    } else if (this.filterType === 'YEAR') {
      this.startDate = dateFns.startOfYear(new Date());
      this.endDate = dateFns.addDays(dateFns.endOfYear(new Date()), 1);
    } else if (this.filterType === 'ALWAYS') {
      this.startDate = dateFns.startOfWeek(new Date(), { weekStartsOn: 1 });

      this._setOverviewFitler$.next({
        startDate: null,
        endDate: null,
      });
      return;
    }
    this._setOverviewFitler$.next({
      startDate: this._formatDateForTransport(this.startDate).toISOString(),
      endDate: this._formatDateForTransport(this.endDate).toISOString(),
    });
  }

  private _initStreams() {
    this.workOverview$ = this._setOverviewFitler$.pipe(
      switchMap(filterDates =>
        filterDates
          ? this._productivitySandbox.getWorkOverview(
              filterDates.startDate,
              filterDates.endDate,
            )
          : of(null),
      ),
    );

    this.periodBarIsSticky$ = new Observable(
      (observer: Observer<IntersectionObserverEntry[]>) => {
        const intersectionObserver = new IntersectionObserver(
          entries => {
            observer.next(entries);
          },
          {
            rootMargin: '-68px 0px 0px 0px',
            threshold: [1],
          },
        );

        intersectionObserver.observe(this.periodBar.nativeElement);

        return () => {
          intersectionObserver.disconnect();
        };
      },
    ).pipe(map(entry => entry[0].intersectionRatio !== 1));
  }

  private _formatDateForTransport(date: Date): Date {
    date.setHours(0, 0, 0, 0);
    return date;
  }

  onSetFilter(filter: 'WEEK' | 'MONTH' | 'YEAR' | 'ALWAYS') {
    this.filterType = filter;

    if (filter === 'WEEK') {
      this.startDate = dateFns.startOfWeek(new Date(), { weekStartsOn: 1 });
      this.endDate = dateFns.addDays(this.startDate, 7);
    } else if (filter === 'MONTH') {
      this.startDate = dateFns.startOfMonth(new Date());
      this.endDate = dateFns.addDays(dateFns.endOfMonth(this.startDate), 1);
    } else if (filter === 'YEAR') {
      this.startDate = dateFns.startOfYear(new Date());
      this.endDate = dateFns.addDays(dateFns.endOfYear(this.startDate), 1);
    } else if (filter === 'ALWAYS') {
      this._setOverviewFitler$.next({
        startDate: null,
        endDate: null,
      });
      return;
    }

    this._setOverviewFitler$.next({
      startDate: this._formatDateForTransport(this.startDate).toISOString(),
      endDate: this._formatDateForTransport(this.endDate).toISOString(),
    });
  }

  onPreviousPeriod() {
    if (this.filterType === 'WEEK') {
      this.startDate = dateFns.subWeeks(this.startDate, 1);
      this.endDate = dateFns.addDays(this.startDate, 7);
    } else if (this.filterType === 'MONTH') {
      this.startDate = dateFns.subMonths(this.startDate, 1);
      this.endDate = dateFns.subMonths(this.endDate, 1);
    } else if (this.filterType === 'YEAR') {
      this.startDate = dateFns.subYears(this.startDate, 1);
      this.endDate = dateFns.subYears(this.endDate, 1);
    }
    this._setOverviewFitler$.next({
      startDate: this._formatDateForTransport(this.startDate).toISOString(),
      endDate: this._formatDateForTransport(this.endDate).toISOString(),
    });
  }

  onNextPeriod() {
    if (this.filterType === 'WEEK') {
      this.startDate = dateFns.addWeeks(this.startDate, 1);
      this.endDate = dateFns.addDays(this.startDate, 7);
    } else if (this.filterType === 'MONTH') {
      this.startDate = dateFns.addMonths(this.startDate, 1);
      this.endDate = dateFns.addMonths(this.endDate, 1);
    } else if (this.filterType === 'YEAR') {
      this.startDate = dateFns.addYears(this.startDate, 1);
      this.endDate = dateFns.addYears(this.endDate, 1);
    }
    this._setOverviewFitler$.next({
      startDate: this._formatDateForTransport(this.startDate).toISOString(),
      endDate: this._formatDateForTransport(this.endDate).toISOString(),
    });
  }
}
