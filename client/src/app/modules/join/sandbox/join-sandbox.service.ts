import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HouseholdService } from 'src/shared/services/household.service';
import { CreateHouseholdFormParams } from 'src/shared/types/join/create-household-form-params';
import { HouseholdCreatedParams } from 'src/shared/types/join/household-created-params';
import { JoinHouseholdParams } from 'src/shared/types/join/join-household-params';
import { HouseholdJoinedParams } from 'src/shared/types/join/household-joined-params';
import { JoinHouseholdAgainParams } from 'src/shared/types/join/join-household-again-params';

@Injectable({
  providedIn: 'root',
})
export class JoinSandboxService {
  constructor(private _householdService: HouseholdService) {}

  joinHousehold(
    params: JoinHouseholdParams,
  ): Observable<HouseholdJoinedParams> {
    return this._householdService.join(params);
  }

  joinHouseholdAgain(
    params: JoinHouseholdAgainParams,
  ): Observable<HouseholdJoinedParams> {
    return this._householdService.joinAgain(params);
  }

  createHousehold(
    params: CreateHouseholdFormParams,
  ): Observable<HouseholdCreatedParams> {
    return this._householdService.create(params);
  }
}
