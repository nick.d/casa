import { TestBed } from '@angular/core/testing';

import { JoinSandboxService } from './join-sandbox.service';

describe('JoinSandboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JoinSandboxService = TestBed.get(JoinSandboxService);
    expect(service).toBeTruthy();
  });
});
