import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Observable, of } from 'rxjs';
import { catchError, take } from 'rxjs/operators';
import { SnackbarService } from 'src/shared/snackbar/services/snackbar.service';
import {
  SnackbarTheme,
  SnackbarType,
} from 'src/shared/snackbar/types/snackbar.type';
import { HouseholdCreatedParams } from 'src/shared/types/join/household-created-params';

import { JoinSandboxService } from '../../sandbox/join-sandbox.service';

@Component({
  selector: 'app-create-household',
  templateUrl: './create-household.component.html',
  styleUrls: ['./create-household.component.scss'],
  animations: [
    trigger('formVisbility', [
      state('visible', style({ transform: 'translateX(0)' })),
      state('hidden', style({ transform: 'translateX(calc(-50% - 1rem))' })),
      transition('visible <=> hidden', [animate('0.5s ease')]),
    ]),
  ],
})
export class CreateHouseholdComponent implements OnInit {
  createHouseholdForm: FormGroup;
  createhouseholdFormVisible = true;
  formScrolledDone = false;
  householdCode: string;
  isCreatingHousehold = false;
  errors: string[];

  constructor(
    private _fb: FormBuilder,
    private _snackbar: SnackbarService,
    private _joinSandbox: JoinSandboxService,
  ) {}

  ngOnInit() {
    this._initForm();
  }

  private _initForm() {
    this.createHouseholdForm = this._fb.group({
      householdName: [
        null,
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(24),
        ],
      ],
      username: [
        null,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(32),
        ],
      ],
      email: [null, [Validators.required, Validators.email]],
    });
  }

  setFormScrollDone() {
    if (!this.createhouseholdFormVisible) {
      this.formScrolledDone = true;
    }
  }

  closeError(error: string) {
    this.errors.splice(this.errors.indexOf(error), 1);
  }

  onCreateHousehold(form: FormGroup) {
    if (form.valid) {
      this.isCreatingHousehold = true;

      this._joinSandbox
        .createHousehold(form.value)
        .pipe(
          take(1),
          catchError(e => this._catchCreateHousholdError(e)),
        )
        .subscribe(params => this._finalizeCreateHoushold(params, form));
    }
  }

  private _finalizeCreateHoushold(
    params: HouseholdCreatedParams,
    form: FormGroup,
  ) {
    this.isCreatingHousehold = false;
    this.householdCode = params.householdCode;
    this.createhouseholdFormVisible = false;

    this._snackbar.open(`${form.value.householdName} werd aangemaakt!`, {
      duration: 5000,
      type: SnackbarType.SUCCESS,
      theme: SnackbarTheme.SUCCESS,
    });
  }

  private _catchCreateHousholdError(e): Observable<HouseholdCreatedParams> {
    if (e.status !== 500 && e.error && e.error.errors) {
      this.errors = e.error.errors.map(error => error.msg);
    }

    this.isCreatingHousehold = false;

    return of<HouseholdCreatedParams>();
  }

  get householdNameControl() {
    return this.createHouseholdForm.get('householdName') as FormControl;
  }

  get usernameControl() {
    return this.createHouseholdForm.get('username') as FormControl;
  }

  get emailControl() {
    return this.createHouseholdForm.get('email') as FormControl;
  }
}
