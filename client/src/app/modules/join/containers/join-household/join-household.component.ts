import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { filter, map, take, takeUntil, catchError } from 'rxjs/operators';
import { CustomValidators } from 'src/shared/validators/custom.validator';
import { Subject, Observable, of } from 'rxjs';
import {
  SnackbarType,
  SnackbarTheme,
} from 'src/shared/snackbar/types/snackbar.type';
import { SnackbarService } from 'src/shared/snackbar/services/snackbar.service';
import { JoinSandboxService } from '../../sandbox/join-sandbox.service';
import { HouseholdJoinedParams } from 'src/shared/types/join/household-joined-params';

@Component({
  selector: 'app-join-household',
  templateUrl: './join-household.component.html',
  styleUrls: ['./join-household.component.scss'],
})
export class JoinHouseholdComponent implements OnInit, OnDestroy {
  joinHouseholdForm: FormGroup;
  errors: string[];
  isJoiningHousehold = false;
  private _destroy$ = new Subject<void>();

  constructor(
    private _fb: FormBuilder,
    private _router: Router,
    private _route: ActivatedRoute,
    private _snackbar: SnackbarService,
    private _joinSandbox: JoinSandboxService,
  ) {}

  ngOnInit() {
    this._initForm();
    this._initStreams();
  }

  ngOnDestroy() {
    this._destroy$.next();
  }

  private _initStreams() {
    this._route.params
      .pipe(
        take(1),
        filter(params => params.householdCode),
        map(({ householdCode }) => householdCode as string),
      )
      .subscribe(householdCode =>
        this.householdCodeControl.patchValue(householdCode.slice(0, 5)),
      );

    this.alreadyMemberControl.valueChanges
      .pipe(takeUntil(this._destroy$))
      .subscribe(_ => {
        this.householdCodeControl.updateValueAndValidity();
        this.usernameControl.updateValueAndValidity();
      });
  }

  private _initForm() {
    this.joinHouseholdForm = this._fb.group({
      alreadyMember: [false],
      householdCode: [
        null,
        [
          Validators.minLength(5),
          Validators.maxLength(5),
          CustomValidators.requiredIf(() => !this.alreadyMemberControl.value),
        ],
      ],
      username: [
        null,
        [
          Validators.minLength(2),
          Validators.maxLength(32),
          CustomValidators.requiredIf(() => !this.alreadyMemberControl.value),
        ],
      ],
      email: [null, [Validators.required, Validators.email]],
    });
  }

  closeError(error: string) {
    this.errors.splice(this.errors.indexOf(error), 1);
  }

  onJoinHousehold(form: FormGroup) {
    if (form.valid) {
      this.isJoiningHousehold = true;

      let joinRequest;

      if (this.alreadyMemberControl.value) {
        joinRequest = this._joinSandbox.joinHouseholdAgain({
          email: this.emailControl.value,
        });
      } else {
        joinRequest = this._joinSandbox.joinHousehold({
          householdCode: this.householdCodeControl.value,
          username: this.usernameControl.value,
          email: this.emailControl.value,
        });
      }

      joinRequest
        .pipe(
          take(1),
          catchError(e => this._catchCreateHousholdError(e)),
        )
        .subscribe(params => this._finalizeCreateHoushold(params));
    }
  }

  private _finalizeCreateHoushold(params: HouseholdJoinedParams) {
    this.isJoiningHousehold = false;

    this._snackbar.open(`Je bent nu lid van ${params.householdName}!`, {
      duration: 5000,
      type: SnackbarType.SUCCESS,
      theme: SnackbarTheme.SUCCESS,
    });

    this._router.navigate(['household', 'overview']);
  }

  private _catchCreateHousholdError(e): Observable<HouseholdJoinedParams> {
    if (e.status !== 500 && e.error && e.error.errors) {
      this.errors = e.error.errors.map(error => error.msg);
    }

    this.isJoiningHousehold = false;

    return of<HouseholdJoinedParams>();
  }

  get alreadyMemberControl() {
    return this.joinHouseholdForm.get('alreadyMember') as FormControl;
  }

  get householdCodeControl() {
    return this.joinHouseholdForm.get('householdCode') as FormControl;
  }

  get usernameControl() {
    return this.joinHouseholdForm.get('username') as FormControl;
  }

  get emailControl() {
    return this.joinHouseholdForm.get('email') as FormControl;
  }
}
