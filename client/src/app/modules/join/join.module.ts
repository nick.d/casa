import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';

import { CreateHouseholdComponent } from './containers/create-household/create-household.component';
import { JoinHouseholdComponent } from './containers/join-household/join-household.component';
import { JoinComponent } from './containers/join/join.component';
import { JoinRoutingModule } from './join-routing.module';
import { SharedModule } from 'src/shared/shared/shared.module';

@NgModule({
  declarations: [
    JoinComponent,
    CreateHouseholdComponent,
    JoinHouseholdComponent,
  ],
  imports: [
    CommonModule,
    JoinRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatIconModule,
    SharedModule,
  ],
})
export class JoinModule {}
