const cors = require("cors");
const express = require("express");
const admin = require("firebase-admin");
const functions = require("firebase-functions");
const jwt = require("jsonwebtoken");
const { check, validationResult } = require("express-validator");
const cryptoRandomString = require("crypto-random-string");
const env = require("./env.js");

const app = express();
app.use(cors({ origin: true }));
app.use(express.json());

if (!env.production) {
  const serviceAccount = require("./key/admin.json");
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://casa-dbe7f.firebaseio.com"
  });
} else {
  admin.initializeApp({
    credential: admin.credential.applicationDefault()
  });
}

let db = admin.firestore();

const isAuthorized = async (req, res, next) => {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "Bearer"
  ) {
    const token = req.headers.authorization.split(" ")[1];

    const payload = jwt.verify(
      token,
      "cbJXSyyJZXwWwXpLcARnI82BPu1Qqxuun9BWfHwR"
    );

    if (payload.email) {
      const userDoc = await db
        .collection("users")
        .where("email", "==", payload.email)
        .get();

      if (!userDoc.empty) {
        if (!!userDoc.docs[0].data().currentHousehold) {
          req.user = { ...userDoc.docs[0].data(), id: userDoc.docs[0].id };

          return next();
        }
      }
    }
  }

  res.status(401).json({
    errors: [
      {
        msg: "UNAUTHORIZED",
        error: "Unauthorized"
      }
    ]
  });
};

app.get("/productivity/overview", isAuthorized, async (req, res) => {
  try {
    let worksDoc;

    if (req.query.startDate && req.query.endDate) {
      const startDate = new Date(req.query.startDate);
      const endDate = new Date(req.query.endDate);

      worksDoc = await db
        .collection("households")
        .doc(req.user.currentHousehold)
        .collection("work")
        .where("atTime", ">=", startDate)
        .where("atTime", "<=", endDate)
        .get();
    } else {
      worksDoc = await db
        .collection("households")
        .doc(req.user.currentHousehold)
        .collection("work")
        .get();
    }

    const membersDoc = await db
      .collection("households")
      .doc(req.user.currentHousehold)
      .collection("members")
      .get();

    const members = membersDoc.docs.map(member => {
      return { ...member.data(), id: member.id };
    });
    let works = members.map(member => {
      return {
        userId: member.id,
        durationInMinutes: 0
      };
    });
    works = [...worksDoc.docs.map(work => work.data()), ...works];

    const worksGroupedByUser = works.reduce((acc, value) => {
      acc[value.userId] = [...(acc[value.userId] || []), value];

      return acc;
    }, {});

    let weeklyOverview = [];

    for (let workGroupName in worksGroupedByUser) {
      weeklyOverview.push({
        user: members.find(member => member.id === workGroupName).username,
        totalMinutes: worksGroupedByUser[workGroupName].reduce((acc, value) => {
          return acc + value.durationInMinutes;
        }, 0)
      });
    }

    weeklyOverview = weeklyOverview.sort(
      (a, b) => b.totalMinutes - a.totalMinutes
    );

    res.json(weeklyOverview);
  } catch (e) {
    res.status(500).json({
      errors: [
        {
          msg: "COULD_NOT_GET_WORK_OVERVIEW",
          error: e.message
        }
      ]
    });
  }
});

app.get("/productivity/weekly-overview", isAuthorized, async (req, res) => {
  try {
    const now = new Date();
    const day = now.getDay(),
      diff = now.getDate() - day + (day === 0 ? -6 : 1);

    const monday = new Date(now.setDate(diff));
    monday.setHours(0, 0, 0, 0);

    const worksDoc = await db
      .collection("households")
      .doc(req.user.currentHousehold)
      .collection("work")
      .where("atTime", ">=", monday)
      .get();

    const membersDoc = await db
      .collection("households")
      .doc(req.user.currentHousehold)
      .collection("members")
      .get();

    const members = membersDoc.docs.map(member => {
      return { ...member.data(), id: member.id };
    });
    let works = members.map(member => {
      return {
        userId: member.id,
        durationInMinutes: 0
      };
    });
    works = [...worksDoc.docs.map(work => work.data()), ...works];

    const worksGroupedByUser = works.reduce((acc, value) => {
      acc[value.userId] = [...(acc[value.userId] || []), value];

      return acc;
    }, {});

    let weeklyOverview = [];

    for (let workGroupName in worksGroupedByUser) {
      weeklyOverview.push({
        user: members.find(member => member.id === workGroupName).username,
        totalMinutes: worksGroupedByUser[workGroupName].reduce((acc, value) => {
          return acc + value.durationInMinutes;
        }, 0)
      });
    }

    weeklyOverview = weeklyOverview.sort(
      (a, b) => b.totalMinutes - a.totalMinutes
    );

    res.json(weeklyOverview);
  } catch (e) {
    res.status(500).json({
      errors: [
        {
          msg: "COULD_NOT_GET_WEEKLY_OVERVIEW",
          error: e.message
        }
      ]
    });
  }
});

app.post(
  "/productivity/register",
  isAuthorized,
  [
    check("durationInMinutes")
      .isInt({ min: 1, max: 300 })
      .withMessage("DURATION_RANGE_NOT_BETWEEN_1_AND_300"),
    check("description")
      .isLength({ min: 1, max: 64 })
      .withMessage("DESCRIPTION_LENGTH_INCORRECT")
      .trim()
      .escape()
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    try {
      await db
        .collection("households")
        .doc(req.user.currentHousehold)
        .collection("work")
        .add({
          userId: req.user.id,
          description: req.body.description,
          durationInMinutes: req.body.durationInMinutes,
          atTime: admin.firestore.Timestamp.fromDate(new Date())
        });

      res.json(true);
    } catch (e) {
      res.status(500).json({
        errors: [
          {
            msg: "COULD_NOT_REGISTER_WORK",
            error: e.message
          }
        ]
      });
    }
  }
);

app.get("/household/info", isAuthorized, async (req, res) => {
  try {
    const doc = db.collection("households").doc(req.user.currentHousehold);

    const household = await doc.get();

    const memberDoc = await doc
      .collection("members")
      .doc(req.user.id)
      .get();

    res.json({
      name: household.data().name,
      code: household.data().code,
      username: memberDoc.data().username
    });
  } catch (e) {
    res.status(500).json({
      errors: [
        {
          msg: "COULD_NOT_GET_HOUSEHOLD_INFO",
          error: e.message
        }
      ]
    });
  }
});

app.get("/household", isAuthorized, async (req, res) => {
  try {
    const doc = db.collection("households").doc(req.user.currentHousehold);
    const household = await doc.get();
    const membersDoc = await doc.collection("members").get();

    res.json({
      name: household.data().name,
      code: household.data().code,
      members: membersDoc.docs.map(member => {
        return { username: member.data().username };
      })
    });
  } catch (e) {
    res.status(500).json({
      errors: [
        {
          msg: "COULD_NOT_GET_HOUSEHOLD_INFO",
          error: e.message
        }
      ]
    });
  }
});

app.get("/user", isAuthorized, async (req, res) => {
  try {
    const memberDoc = await db
      .collection("households")
      .doc(req.user.currentHousehold)
      .collection("members")
      .doc(req.user.id)
      .get();

    res.json({
      username: memberDoc.data().username,
      email: memberDoc.data().email
    });
  } catch (e) {
    res.status(500).json({
      errors: [
        {
          msg: "COULD_NOT_GET_HOUSEHOLD_INFO",
          error: e.message
        }
      ]
    });
  }
});

app.post(
  "/household/join-again",
  [
    check("email")
      .isEmail()
      .withMessage("INVALID_EMAIL")
      .trim()
      .custom(async value => {
        const doc = await db
          .collection("users")
          .where("email", "==", value)
          .get();

        if (doc.empty) {
          return Promise.reject("NO_USER_WITH_EMAIL");
        }

        if (!doc.docs[0].data().currentHousehold) {
          return Promise.reject("NO_CURRENT_HOUSEHOLD");
        }
      })
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    try {
      const userDoc = await db
        .collection("users")
        .where("email", "==", req.body.email)
        .get();

      const householdDoc = await db
        .collection("households")
        .doc(userDoc.docs[0].data().currentHousehold)
        .get();

      res.json({
        householdName: householdDoc.data().name,
        token: jwt.sign(
          { email: req.body.email },
          "cbJXSyyJZXwWwXpLcARnI82BPu1Qqxuun9BWfHwR"
        )
      });
    } catch (e) {
      res.status(500).json({
        errors: [
          {
            msg: "COULD_NOT_JOIN_HOUSEHOLD_AGAIN",
            error: e.message
          }
        ]
      });
    }
  }
);

app.post(
  "/household/join",
  [
    check("email")
      .isEmail()
      .withMessage("INVALID_EMAIL")
      .trim(),
    check("username")
      .isLength({ min: 2, max: 32 })
      .withMessage("USERNAME_LENGTH_INCORRECT")
      .trim()
      .escape(),
    check("householdCode")
      .trim()
      .isLength({ min: 5, max: 5 })
      .withMessage("HOUSECODE_LENGTH_INCORRECT")
      .custom(async value => {
        const snapshot = await db
          .collection("households")
          .where("code", "==", value)
          .get();

        if (snapshot.empty) {
          return Promise.reject("HOUSEHOLD_NOT_FOUND");
        }
      })
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    try {
      const snapshot = await db
        .collection("households")
        .where("code", "==", req.body.householdCode)
        .get();

      const usersDoc = await db
        .collection("users")
        .where("email", "==", req.body.email)
        .get();

      let userId;

      if (usersDoc.empty) {
        const newUserDoc = await db.collection("users").add({
          email: req.body.email,
          currentHousehold: snapshot.docs[0].id
        });

        userId = newUserDoc.id;
      } else {
        userId = usersDoc.docs[0].id;

        await db
          .collection("users")
          .doc(userId)
          .set({
            email: req.body.email,
            currentHousehold: snapshot.docs[0].id
          });
      }

      await db
        .collection("households")
        .doc(snapshot.docs[0].id)
        .collection("members")
        .doc(userId)
        .set({ email: req.body.email, username: req.body.username });

      res.json({
        householdName: snapshot.docs[0].data().name,
        token: jwt.sign(
          { email: req.body.email },
          "cbJXSyyJZXwWwXpLcARnI82BPu1Qqxuun9BWfHwR"
        )
      });
    } catch (e) {
      res.status(500).json({
        errors: [
          {
            msg: "COULD_NOT_JOIN_HOUSEHOLD",
            error: e.message
          }
        ]
      });
    }
  }
);

app.post(
  "/household/create",
  [
    check("email")
      .isEmail()
      .withMessage("INVALID_EMAIL")
      .trim(),
    check("username")
      .isLength({ min: 2, max: 32 })
      .withMessage("USERNAME_LENGTH_INCORRECT")
      .trim()
      .escape(),
    check("householdName")
      .isLength({ min: 3, max: 24 })
      .withMessage("HOUSEHOLD_NAME_LENGTH_INCORRECT")
      .trim()
      .escape()
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    try {
      const householdCode = cryptoRandomString({ length: 5, type: "url-safe" });

      const householdDoc = await db.collection("households").add({
        name: req.body.householdName,
        code: householdCode,
        lastActivity: admin.firestore.Timestamp.fromDate(new Date())
      });

      const userDoc = await db.collection("users").add({
        email: req.body.email,
        currentHousehold: householdDoc.id
      });

      await householdDoc
        .collection("members")
        .doc(userDoc.id)
        .set({
          email: req.body.email,
          username: req.body.username
        });

      res.json({
        householdCode,
        token: jwt.sign(
          { email: req.body.email },
          "cbJXSyyJZXwWwXpLcARnI82BPu1Qqxuun9BWfHwR"
        )
      });
    } catch (e) {
      res.status(500).json({
        errors: [
          {
            msg: "COULD_NOT_CREATE_HOUSEHOLD",
            error: e.message
          }
        ]
      });
    }
  }
);

exports.api = functions.https.onRequest(app);
